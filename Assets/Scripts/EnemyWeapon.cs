using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour
{
    [SerializeField]
    int attackDamage_ = 10; // 攻撃力
    [SerializeField]
    BoxCollider hitBox_ = null;
    [SerializeField]
    PlayerController playerController_ = null;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    // 当たり判定を有効化する
    public void ActiveCollider()
    {
        hitBox_.enabled = true;
    }

    // 当たり判定を無効化する
    public void InactiveCollider()
    {
        hitBox_.enabled = false;
    }
    // 攻撃
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerController_.ReceiveDamage(attackDamage_);
        }
    }
}
