using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using UnityEngine.AI;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField]
    int maxHP_; // 最大hp
    int currentHP_; // 現在のhp
    [SerializeField]
    Slider hpSlider_ = null; // hpを視覚的に表示
    [SerializeField]
    Animator anim_ = null; // アニメーション
    [SerializeField]
    EnemyAI enemyAI_ = null;
    bool isInvincible_ = false; // 無敵状態 ダメージを受けない

    
    // Start is called before the first frame update
    void Start()
    {

        Assert.IsNotNull(hpSlider_, "hpSlider_にオブジェクトをアタッチしてください");
        Assert.IsNotNull(anim_, "anim_にオブジェクトをアタッチしてください");

        currentHP_ = maxHP_;
        hpSlider_.maxValue = maxHP_;
        hpSlider_.value = currentHP_;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // ダメージを受ける
    public void ReceiveDamage(int damage)
    {
        if(!isInvincible_)
        {
            enemyAI_.TurnOffTrigger();
            enemyAI_.isHurt = true;
            currentHP_ = Mathf.Max(0, currentHP_ - damage);
            hpSlider_.value = currentHP_;

            // 死亡時アニメーション分岐
            if (currentHP_ <= 0)
            {
                enemyAI_.TurnOffTrigger();
                enemyAI_.OnDead();
                anim_.SetTrigger("Dead");
                Destroy(this.gameObject, 5.0f);
            }
            else
            {
                // ダメージ受けただけ
                anim_.SetTrigger("GetHit");
            }

            // 無敵時間
            isInvincible_ = true;
        }
        
    }

    public void Invicible()
    {
        enemyAI_.isHurt = false;
        isInvincible_ = false;
    }

}
