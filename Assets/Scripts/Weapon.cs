using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Weapon : MonoBehaviour
{
    
    [SerializeField]
    BoxCollider hitBox_ = null;

    // Start is called before the first frame update
    void Start()
    {
        Assert.IsNotNull(hitBox_, "hitBox_にオブジェクトがアタッチされていません");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // 当たり判定を有効化する
    public void ActiveCollider()
    {
        hitBox_.enabled = true;
    }

    // 当たり判定を無効化する
    public void InactiveCollider()
    {
        hitBox_.enabled = false;
    }

    // 攻撃
    private void OnTriggerEnter(Collider other)
    {
        EnemyHealth enemyHealth = other.gameObject.GetComponent<EnemyHealth>();
        if (enemyHealth != null)
        {
            enemyHealth.ReceiveDamage(10);
        }
    }
}
