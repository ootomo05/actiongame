using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.AI;
using UnityEngine.Assertions;

public class EnemyAI : MonoBehaviour
{
    // コンポーネント
    [SerializeField]
    Animator anim_ = null;

    // AI
    NavMeshAgent navMeshAgent_ = null;
    [SerializeField]
    GameObject[] targetObject_ = new GameObject[3]; // 目標位置
    int targetIndex_ = 0;
    [SerializeField]
    GameObject player_ = null; // プレイヤーにアクセス
    PlayerController playerController_ = null;
    [SerializeField]
    EnemyWeapon weapon_ = null;
    // ステータス
    [SerializeField]
    float walkSpeed_ = 0.0f; // 歩きスピード
    [SerializeField]
    float runSpeed_ = 0.0f;  // 走りスピード
    [SerializeField]
    float sightLength_ = 0f; // 視野の長さ


    public bool isHurt = false; // ダメージを受けている

    // AIがなる状態
    enum AI_STATE
    {
        IDLE,
        PATROL,
        WANDER,
        ATTACK,
        CHASE,
        DEAD,
        MAX
    }
    AI_STATE state_ = AI_STATE.IDLE; // enemyの状態

    // AIの状態によって実行する関数を変更する
    private Action[] stateActionList_ = null;


    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent_ = GetComponent<NavMeshAgent>();
        Assert.IsNotNull(targetObject_, "targetObject_が設定されていません");
        state_ = AI_STATE.PATROL;
        SetStateAction();

        playerController_ = player_.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isHurt)
        {
            // 現在の状態によって実行する関数を変更
            stateActionList_[(int)state_]();
        }
    }

    // 行動の登録
    // 引数 なし
    void SetStateAction()
    {
        stateActionList_ = new Action[(int)AI_STATE.MAX];
        stateActionList_[(int)AI_STATE.IDLE] = Idle;
        stateActionList_[(int)AI_STATE.PATROL] = Patrol;
        stateActionList_[(int)AI_STATE.WANDER] = Wander;
        stateActionList_[(int)AI_STATE.ATTACK] = Attack;
        stateActionList_[(int)AI_STATE.CHASE] = Chase;
        stateActionList_[(int)AI_STATE.DEAD] = Dead;
    }

    // 初期状態
    void Idle()
    {
        
    }

    void Patrol()
    {
        // 目的地の設定
        if (!navMeshAgent_.hasPath)
        {
            // 目的地の設定
            navMeshAgent_.SetDestination(targetObject_[targetIndex_ % targetObject_.Length].transform.position);
            targetIndex_ = (targetIndex_ + 1) % targetObject_.Length;
            navMeshAgent_.speed = walkSpeed_;
            TurnOffTrigger();
            anim_.SetBool("isWalk", true);
        }

        // プレイヤーを見つけた
        if (CanSeePlayer() && !playerController_.IsDead())
        {
            state_ = AI_STATE.CHASE;
            navMeshAgent_.ResetPath();
        }
    }

    // 索敵
    void Wander()
    {
    }

    // 攻撃
    void Attack()
    {
        // ターゲットがいる方向に向きを変える
        transform.LookAt(new Vector3(player_.transform.position.x, transform.position.y, player_.transform.position.z));
        // TurnOffTrigger();
        anim_.SetBool("isAttack", true);

        if (DistanceToPlayer() >= navMeshAgent_.stoppingDistance + 1)
        {
            TurnOffTrigger();
            state_ = AI_STATE.CHASE;
        }

        // プレイヤーが死亡していたら
        if (playerController_.IsDead())
        {
            TurnOffTrigger();
            state_ = AI_STATE.PATROL;
        }
    }

    // 追跡
    void Chase()
    {
       
        navMeshAgent_.SetDestination(player_.transform.position);
        navMeshAgent_.stoppingDistance = 2;
        navMeshAgent_.speed = runSpeed_;

        TurnOffTrigger();

        anim_.SetBool("isRun", true);

        // 攻撃状態に以降
        if (DistanceToPlayer() < navMeshAgent_.stoppingDistance + 1)
        {
            state_ = AI_STATE.ATTACK;
            TurnOffTrigger();
        }

        // プレイヤーを見失う
        if (!CanSeePlayer())
        {
            navMeshAgent_.ResetPath();
            state_ = AI_STATE.PATROL;
        }

        Vector3 target = player_.transform.position - transform.position;
        float distanc = target.magnitude;
    }

    // 死亡
    void Dead()
    {
    }

    // プレイヤーを見つけることができるか
    // 引数　なし
    // 戻値 プレイヤーが見える位置にいればtrueを返す
    bool CanSeePlayer()
    {
        if (DistanceToPlayer() < sightLength_)
        {
            return true;
        }
        return false;
    }

    // プレイヤーとの距離を図る
    float DistanceToPlayer()
    {
        return Vector3.Distance(player_.transform.position, transform.position);
    }

    // 全てのアニメーションを停止
    public void TurnOffTrigger()
    {
        anim_.SetBool("isWalk", false);
        anim_.SetBool("isRun", false);
        anim_.SetBool("isAttack", false);
    }

    // 死亡
    public void OnDead()
    {
        state_ = AI_STATE.DEAD;
    }

    public void ActiveCollider()
    {
        weapon_.ActiveCollider();
    }

    public void InactiveCollider()
    {
        weapon_.InactiveCollider();
    }
}

