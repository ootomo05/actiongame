using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
public class FuncReceiver : MonoBehaviour
{
    [SerializeField]
    Weapon weapon_ = null;
    [SerializeField]
    PlayerController playerController_ = null;
    // アニメーションにほかのクラスの関数を渡してやる
    // Start is called before the first frame update
    void Start()
    {
        Assert.IsNotNull(playerController_, "playerControllerにPlayerContorllerがアタッチされていません");
        Assert.IsNotNull(weapon_, "Weaponクラスをセットして下さい");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EndAttack()
    {
        playerController_.EndAttack();
    }

    public void ActiveCollider()
    {
        weapon_.ActiveCollider();
    }

    public void InactiveCollider()
    {
        weapon_.InactiveCollider();
    }

    // 無敵状態解除
    public void LiftInvicible()
    {
        playerController_.SetIsInvicible(false);
    }
}
