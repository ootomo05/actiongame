using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    // コンポーネント取得用変数
    Rigidbody rb_ = null;   // 剛体
    [SerializeField]
    Camera cam_ = null;     // カメラ
    [SerializeField]
    Animator anim_ = null; // アニメーション
    [SerializeField]
    Slider hpSlider_ = null; // hpを視覚的に表示
    
    // キャラクターステータス
    [SerializeField]
    int maxHP_ = 100;     // 最大体力
    int currentHP_ = 0;   // 現在の体力
    [SerializeField]
    float walkSpeed_ = 0.0f; // 歩き時の速度
    [SerializeField]
    float runSpeed_ = 0.0f;  // 走り時の速度
    float currentSpeed_ = 0.0f;  // 移動速度

    bool isDead_ = false; // 死亡判定
    bool isInvincible_ = false; // 無敵状態 ダメージを受けない
    bool isAttack_ = false; // 攻撃中
    private Vector3 latestPos_; // 前回のPosition
    float moveDirX_ = 0.0f; // 移動方向
    float moveDirZ_ = 0.0f; // 移動方向

    // Start is called before the first frame update
    void Start()
    {
        // コンポーネント取得
        rb_ = GetComponent<Rigidbody>();
        Assert.IsNotNull(cam_, "cam_にCameraオブジェクトがアタッチされてません");
        Assert.IsNotNull(anim_, "anim_にAnimatorがアタッチされてません");
        Assert.IsNotNull(hpSlider_, "hpSlider_にSliderUIがアタッチされてません");
        
        // hpの初期化処理
        currentHP_ = maxHP_;
        hpSlider_.maxValue = maxHP_;
        hpSlider_.value = maxHP_;

        latestPos_ = transform.position;

        currentSpeed_ = walkSpeed_;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isAttack_ && !isDead_)
        {
            Move();
            Attack();
            if (Input.GetKeyDown(KeyCode.P))
            {
                ReceiveDamage(10);
            }
        }
    }

    // 移動
    // 引数 なし
    private void Move()
    {
        // キー入力の取得
        moveDirX_ = Input.GetAxisRaw("Horizontal");
        moveDirZ_ = Input.GetAxisRaw("Vertical");
    
        // 移動
        // カメラが向いている方向を軸に移動方向を決める
        Vector3 moveDir = cam_.transform.forward * moveDirZ_ + cam_.transform.right * moveDirX_;
        moveDir.y = 0; // y軸で移動しないようにする
        rb_.velocity = moveDir.normalized * currentSpeed_; // 移動速度を一定にするために正規化

        // キャラが移動している方向に向きを変える
        Vector3 diff = transform.position - latestPos_;
        latestPos_ = transform.position;

        if (diff.magnitude > 0.01f)
        {
            transform.rotation = Quaternion.LookRotation(diff);
        }

        // アニメーション
        anim_.SetBool("isWalk", IsWalking());

        Run();
    }


    // 走る
    // 引数 なし
    private void Run()
    {
        if (Input.GetKey(KeyCode.LeftShift) && IsWalking())
        {
            anim_.SetBool("isRun", true);
            currentSpeed_ = runSpeed_;
        }
        else
        {
            anim_.SetBool("isRun", false);
            currentSpeed_ = walkSpeed_;
        }
    }

    private bool IsWalking()
    {
        return Mathf.Abs(moveDirX_) > 0 || Mathf.Abs(moveDirZ_) > 0;
    }
    // 攻撃
    // 引数 なし
    private void Attack()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isAttack_ = true;
            anim_.SetTrigger("Attack");
        }
    }

    // ダメージを受ける
    // 引数 damage 受けるダメージ量
    public void ReceiveDamage(int damage)
    {
        if (!isInvincible_)
        {
            currentHP_ = Mathf.Max(0, currentHP_ - damage); // 体力が0より下がらないようにする
            hpSlider_.value = currentHP_; // uiの更新

            anim_.SetTrigger("GetHit");

            // 体力が0以下になったら死亡
            if (currentHP_ <= 0)
            {
                isDead_ = true;
                anim_.SetTrigger("Dead");
                Invoke("GameOver", 3);
            }

            // アニメーション終了まで無敵になる
            isInvincible_ = true;
        }
    }

    // 体力を回復する
    // 引数 
    public void Heal()
    {
        currentHP_ = Mathf.Min(currentHP_ + 10, maxHP_); // 最大hpより回復しないようにする
        hpSlider_.value = currentHP_; // uiの更新
    }


    // 攻撃終了
    public void EndAttack()
    {
        isAttack_ = false;
    }

    public void SetIsInvicible(bool invincible_)
    {
        isInvincible_ = invincible_;
    }

    // ゲームオーバーシーンへ遷移
    void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }

    public bool IsDead()
    {
        return isDead_;
    }
}
