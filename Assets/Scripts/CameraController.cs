using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // 角度制限
    float maxAngleX_ = 60f;
    float minAngleX_ = -60f;

    [SerializeField] GameObject player_ = null;
    [SerializeField] Camera cam_ = null;

    [SerializeField] float rotateSpeed_ = 3f;
    [SerializeField] Vector3 axisPos_;


    // Start is called before the first frame update
    void Start()
    {
        cam_.transform.localPosition = new Vector3(0, 0, -3);
        cam_.transform.localRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        // カメラ回転
        transform.position = player_.transform.position + axisPos_;
        transform.eulerAngles += new Vector3(Input.GetAxis("Mouse Y") * rotateSpeed_,
            Input.GetAxis("Mouse X") * rotateSpeed_, 0);

        float angleX = transform.eulerAngles.x;
        if (angleX >= 180)
        {
            angleX = angleX - 360;
        }

        transform.eulerAngles = new Vector3(
            Mathf.Clamp(angleX, minAngleX_, maxAngleX_),
            transform.eulerAngles.y,
            transform.eulerAngles.z);
    }
}
